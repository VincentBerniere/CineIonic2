import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { MovieService } from '../../providers/movie-service';
import { Info } from '../info/info';

/*
 Generated class for the Popular page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-popular',
  templateUrl: 'popular.html',
  providers: [MovieService]
})
export class Popular {
  movies:any = [];
  loader: any;
  page: any;
  searchPage: any;

  constructor(public navCtrl: NavController, public movieService: MovieService, public loadingCtrl: LoadingController) {
    this.searchPage = null;
    this.page = 1;
    this.presentLoading();
    this.loadPopularMovies(1);
  }

  loadPopularMovies(page) {
    this.movieService.popular(page)
      .then(data => {
        for(let movie of data['results']) {
          this.movies.push(movie);
        }
        this.searchPage = null;
        this.loader.dismiss();
      });
  }

  loadInfoMovie(id) {
    this.navCtrl.push(Info, {
      idMovie: id
    });
  }

  loadSearchMovies(query, page) {
    this.movieService.search(query, page)
      .then(data => {
        for(let movie of data['results']) {
          this.movies.push(movie);
        }
        this.searchPage = query;
      });
  }

  onSearchInput($event) {
    console.log('Async operation has start');
    let str = $event.target.value;
    setTimeout(() => {
      this.movies = [];
      this.page = 1;
      if (str != '' && str != undefined) {
        this.loadSearchMovies(str, this.page);
      }
    }, 3000);
  }

  addMovie(id) {
    console.log("Add movie : "+id);
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loader.present();
  }

  doInfinite(infiniteScroll:any) {
    console.log('Begin async operation');

    setTimeout(() => {
      if (this.searchPage == null) {
        this.loadPopularMovies(++this.page);
      } else {
        this.loadSearchMovies(this.searchPage, ++this.page);
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }

}
