import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { SocialSharing } from 'ionic-native';
import { MovieService } from '../../providers/movie-service';

/*
  Generated class for the Info page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
  providers: [MovieService]
})
export class Info {
  movie: any;
  loader: any;
  movies: any = [];
  similars: any = [];

  constructor(public navCtrl: NavController, public movieService: MovieService, private navParams: NavParams, public loadingCtrl: LoadingController, private toastCtrl: ToastController) {
    this.presentLoading();
    var id = navParams.get('idMovie');
    this.loadMovie(id);
    this.similarMovies(id, 1);
    this.loadMoreSimilarMovies(5);
  }

  loadMovie(id) {
    this.movieService.info(id)
      .then(data => {
        this.movie = data;
      });
  }

  similarMovies(id, page) {
    console.log("load all similar movies");
    this.movieService.similar(id,page)
      .then(data => {
        for(let movie of data['results']) {
          this.movies.push(movie);
        }
        this.loader.dismiss();
      });
  }

  loadMoreSimilarMovies(nbMore) {
    console.log("load "+nbMore+" similar movies");
    for(let movie in this.movies) {
      console.log(movie);
      this.similars.push(movie);
      this.movies.remove(movie);
    }
  }

  loadOtherMovie(id) {
    this.navCtrl.pop();
    this.navCtrl.push(Info, {
      idMovie: id
    });
  }

  share(social) {
    let subject = "CineIonic";
    let message = "Have you ever seen : "+this.movie['title']+" ?";
    let image = "http://image.tmdb.org/t/p/w500"+this.movie['poster_path'];
    let url = "https://www.themoviedb.org/movie/" + this.movie['id'];

    SocialSharing.share(message, subject, image, url).then(() => {
    }).catch(() => {
      this.presentToast("Error: There was a problem when sharing !");
    });
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    this.loader.present();
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  doInfinite(infiniteScroll:any) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.loadMoreSimilarMovies(5);

      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }

}
