import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { Popular } from '../pages/popular/popular';
import { Info } from '../pages/info/info';

@NgModule({
  declarations: [
    MyApp,
    Popular,
    Info
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Popular,
    Info
  ],
  providers: []
})
export class AppModule {}
